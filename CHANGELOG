
Lightwhale Changelog
====================

This log only contains development headlines in the Lightwhale
repository; it is by no means an exhaustive list of what has changed
between releases.  For details, please see the Buildroot CHANGELOG:
https://git.buildroot.net/buildroot/tree/CHANGES



2.1.4  @  2024-04-01
--------------------

 - Fixed double DHCP client bug where two different DHCP clients
   would start.  Now dhcpcd has been removed, and only udhcpc exist.

 - Removed unused GRUB files in rootfs.



2.1.3  @  2024-02-18
--------------------

 - Added support for mounting LVMs for persistence.  However, to
   actually use LVM, you have to manually construct the VG and LV
   as Lightwhale will not automatically do this.

 - Scan all drives and all partitions for magic labels, allow for
   greater flexibility, e.g. using LVM or other prepared partitioning
   schemes.



2.1.2  @  2024-01-31
--------------------

 - Fixed dockerd now given sufficient time to properly stop all
   containers before system shutdown, otherwise potentially causing
   data loss or corruption!
   Now using full-blown, non-busybox start-stop-daemon with --retry.

 - Fixed setup-persistence now writing to its log file during init.

 - Added kernel modules that allows Lightwhale to run as a
   virtualized guest under VMWare.

 - Added kernel modules that allows Lightwhale to run as a
   virtualized guest under Xen.

 - Added kernel modules that allows Lightwhale to run as a host
   to run virtualized guests under KVM.
   Note that an actual virtualizer e.g. QEMU was not added.

 - Added support for using virtio block devices for persistence,
   e.g. /dev/vda under Proxmox.



2.1.1  @  2024-01-14
--------------------

 - Added Docker buildx support.

 - Fixed Vim warnings caused by missing include syntax files.
   Added empty placeholders for syntax files that are deemed too
   large or unnecessary to be included.



2.1.0  @  2023-12-31
--------------------

 - Combined BIOS and EFI images into a single multi-bootable ISO.

 - Added last missing kernel config for Docker Swarm to fully work on
   Lightwhale.  This also resulted in my first PR to the Moby project:
   https://github.com/moby/moby/pull/46667#event-10699642869

 - Baked nearly all networking and filtering modules into the kernel
   that are required by Docker.  This frees up memory that was wasted
   on modules included in the rootfs and also always loaded into
   memory anyway.  This increases the size of the kernel slightly but
   reduces the rootfs even more.  Also, now lsmod is less cluttered.

 - Added bash completion for docker and other commands.

 - Added P9 networking file system and various virtio features to
   better support virtualizing of Lightwhale under QEMU and Proxmox.

 - Bumped to GRUB 2.12 to be able to load large rootfs under EFI.

 - Switched to cgroupsfs v2 because of its cleaner mount.

 - Bumped to Buildroot 2023.02.8



2.0.4  @  2023-09-03
--------------------

 - Fixed machine-id generator to include mother board serial number
   and network card MAC address as intended.  Identical hardware could
   otherwise result in equal machine-ids.  If you're currently relying
   on a unique /etc/machine-id, you're advised to regenerate:

     sudo /lib/lightwhale/machine-id | sudo tee /etc/machine-id



2.0.3  @  2023-08-19
--------------------

 - Added /etc/machine-id, containing a unique hash key for machine
   identification, generated from various hardware properties.

 - Remove invalid characters from hostname and machine-id when given
   on the boot prompt. 



2.0.2  @  2023-08-09
--------------------

 - Added UTF-8 support, enabling it by default.

 - Added kernel modules for Realtek network interface cards.



2.0.1  @  2023-07-24
--------------------

 - Fixed persistence to support NVME and eMMC devices too.

 - Removed wget because it didn't support https, and adding it would
   require increase image size disproportionately.  Use curl.



2.0.0  @  2023-07-19
--------------------

- Updated to Buildroot 2023.02.2 LTS.

- Replaced dropbear with openssh sshd.  Clients should delete their
  old Lightwhale entry in known_hosts, or else they will be reluctant
  to connect due to remote host identification change.

- Refactored rootfs to use squashfs and be truely immutable.

- Refactored overlaying so tmpfs is mounted as writable overlay when
  persistence is not enabled.

- Add swap partition next to persistence partition when claiming
  persistence device.

- Refactored image build system and boot scripts.

- Fixed bug where Lightwhale could be configured for wifi networks
  that didn't require password (alias open networks).



1.2.1  @  2022-07-13
--------------------

- Fixed bug where docker data dir would not become properly
  reconfigured for persistence.



1.2.0  @  2022-05-28
--------------------

- Added tun module for network tunneling.

- Updated docker-compose to version 2.5.1.



1.1.0  @  2022-03-21
--------------------

- Docker no longer run with remapped user ids (--userns-remap),
  and now has the same, expected behavior as a default docker
  installation.  However, the hardened security option can easily
  be enabled again.  See details in /etc/default/docker.

- Added more ethernet network device drivers.



1.0.1  @  2022-03-01
--------------------

- Added gzip, bzip2, and xz tools, including zgrep for grepping
  compressed, rotated logs.





Acknowledgements
================

Thanks, Minimons, for reporting and testing persistence on virtio block devices.
Thanks, Krystle, for feedback, particularly for fixing graceful dockerd shut down.
Thanks, Rebek Kahn, for the nice suggestions.
Thanks, Minimons, for reporting and testing EFI boot.
Thanks, Leonard Hansen, for point out wget was broken.
Thanks, Brian Ketelsen, for reporting and testing NVME persistence.
Thanks, stam, for reporting and testing Realtek NIC drivers.
