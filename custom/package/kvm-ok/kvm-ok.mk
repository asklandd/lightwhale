################################################################################
#
# kvm-ok
# https://bazaar.launchpad.net/~cpu-checker-dev/cpu-checker/trunk/files/37
#
################################################################################

KVM_OK_VERSION = 37
#KVM_OK_SITE = https://bazaar.launchpad.net/~cpu-checker-dev/cpu-checker/trunk/download/$(KVM_OK_VERSION)/kvm-ok
KVM_OK_SITE = https://bazaar.launchpad.net/~cpu-checker-dev/cpu-checker/trunk/tarball/$(KVM_OK_VERSION)
KVM_OK_LICENSE = GPL-3+
KVM_OK_LICENSE_FILES = LICENCE

define KVM_OK_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/cpu-checker/trunk/kvm-ok $(TARGET_DIR)/usr/bin
endef

$(eval $(generic-package))
