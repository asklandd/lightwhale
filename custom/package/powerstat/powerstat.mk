################################################################################
#
# powerstat
# https://github.com/ColinIanKing/powerstat
#
################################################################################

LIGHTHWALE_POWERSTAT_VERSION = V0.03.03
LIGHTHWALE_POWERSTAT_SITE = $(call github,ColinIanKing,powerstat,$(POWERSTAT_VERSION))
LIGHTHWALE_POWERSTAT_LICENSE = GPLv2
LIGHTHWALE_POWERSTAT_LICENSE_FILES = LICENCE
LIGHTHWALE_POWERSTAT_CONF_OPTS = --without-docs

define LIGHTWHALE_POWERSTAT_BUILD_CMDS
    $(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D)
endef

define LIGHTWHALE_POWERSTAT_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/powerstat $(TARGET_DIR)/usr/bin
endef

$(eval $(generic-package))
