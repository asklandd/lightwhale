################################################################################
#
# docker-buildx
#
################################################################################

DOCKER_BUILDX_VERSION = 0.12
DOCKER_BUILDX_SITE = $(call github,docker,buildx,v$(DOCKER_BUILDX_VERSION))
DOCKER_BUILDX_LICENSE = Apache-2.0
DOCKER_BUILDX_LICENSE_FILES = LICENSE

DOCKER_BUILDX_GOMOD = github.com/docker/buildx
DOCKER_BUILDX_TAGS = cgo
DOCKER_BUILDX_LDFLAGS = -X $(DOCKER_BUILDX_GOMOD)/version.Version="$(DOCKER_BUILDX_VERSION)"

DOCKER_BUILDX_BUILD_TARGETS = cmd/buildx
DOCKER_BUILDX_INSTALL_BINS = $(notdir $(DOCKER_BUILDKIT_BUILD_TARGETS))

define DOCKER_BUILDX_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/bin/buildx $(TARGET_DIR)/usr/lib/docker/cli-plugins/docker-buildx
endef

$(eval $(golang-package))
