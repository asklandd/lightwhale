#!/bin/bash

this="${0##*/}"

echo "###############  Hello, I'm $this"
set -uex

BUILD_DIR=${BUILD_DIR:-$O/build}
BINARIES_DIR=${BINARIES_DIR:-$O/images}
HOST_DIR=${HOST_DIR:-$O/host}
BR2_EXTERNAL_LIGHTWHALE_PATH=${BR2_EXTERNAL_LIGHTWHALE_PATH:-$LIGHTWHALE_SDK_CONTAINER_HOME/custom}

SRC=$(dirname $0)
SYSLINUX_BUILD_DIR=$BUILD_DIR/syslinux-6.03
SYSLINUX_PREBUILT_DIR=$SRC/syslinux/prebuilt
GRUB_BUILD_DIR=$BUILD_DIR/grub2-2.12
CONFIG_SRC=$SRC/genimage-esp.cfg

BASE=$BINARIES_DIR/lightwhale-iso
ROOT=$BASE/root
TEMP=$BASE/tmp
IN=$BASE/in
OUT=$BASE/out
CONFIG=$IN/genimage-esp.cfg
[ ! -d $BASE ] || rm -frv $BASE

mkdir -pv $ROOT $TEMP $IN $OUT
mkdir -pv $IN/iso/boot/isolinux $IN/iso/boot/grub/fonts $IN/esp/EFI/BOOT #$IN/iso/EFI/BOOT


# Build GRUB boot loader
GRUB_MODULES_MINIMAL=(
    all_video
    boot
    cat
    efi_gop
    echo
    fat
    font
    gfxterm
    iso9660
    linux ls
    normal part_gpt part_msdos read regexp
    search search_fs_file sleep
)

GRUB_MODULES_BLOATED=(
    all_video at_keyboard boot btrfs cat chain configfile
    echo efifwsetup efinet exfat ext2 f2fs fat font
    gfxmenu gfxterm gzio halt hfsplus iso9660 jpeg keylayouts
    linux loadenv loopback lsefi lsefimmap minicmd
    normal ntfs ntfscomp part_apple part_gpt part_msdos png read reboot regexp
    search search_fs_file search_fs_uuid search_label serial sleep tpm
    udf usb usbserial_common usbserial_ftdi usbserial_pl2303 usbserial_usbdebug
    video video_colors video_bochs video_cirrus video_fb videotest videoinfo xfs zstd
)


$HOST_DIR/bin/grub-mkimage \
    -v \
    -d $GRUB_BUILD_DIR/build-x86_64-efi/grub-core/ \
    -O x86_64-efi \
    -o $OUT/BOOTx64-minimal.EFI \
    -p "/EFI/BOOT" \
    -c $SRC/grub/grub-builtin.cfg \
    ${GRUB_MODULES_MINIMAL[@]}

$HOST_DIR/bin/grub-mkimage \
    -v \
    -d $GRUB_BUILD_DIR/build-x86_64-efi/grub-core/ \
    -O x86_64-efi \
    -o $OUT/BOOTx64-bloated.EFI \
    -p "/EFI/BOOT" \
    -c $SRC/grub/grub-builtin.cfg \
    ${GRUB_MODULES_BLOATED[@]}

ls -lh $OUT/BOOTx64*.EFI



# Install Lightwhale images and version file.
cp -av $BINARIES_DIR/bzImage									$IN/iso/boot/lightwhale-kernel
cp -av $BINARIES_DIR/rootfs.squashfs							$IN/iso/boot/lightwhale-rootfs
cp -av $BINARIES_DIR/lightwhale-release							$IN/iso/boot/lightwhale-release

# Install SYSLINUX.
# This syslinux is broken as it ignores all inputs incl. kernel options and always boots default menu entry.
#cp -av $SYSLINUX_BUILD_DIR/bios/mbr/isohdpfx.bin                    $IN/
#cp -av $SYSLINUX_BUILD_DIR/bios/core/isolinux.bin                   $IN/iso/boot/isolinux/
#cp -av $SYSLINUX_BUILD_DIR/bios/com32/elflink/ldlinux/ldlinux.c32   $IN/iso/boot/isolinux/
# The pre-built works.
cp -av $SYSLINUX_PREBUILT_DIR/bios/mbr/isohdpfx.bin                  $IN/
cp -av $SYSLINUX_PREBUILT_DIR/bios/core/isolinux.bin                 $IN/iso/boot/isolinux/
cp -av $SYSLINUX_PREBUILT_DIR/bios/com32/elflink/ldlinux/ldlinux.c32 $IN/iso/boot/isolinux/
cp -av $SRC/syslinux/help-*.txt                                      $IN/iso/boot/isolinux/
cp -av $BINARIES_DIR/syslinux-help-release.txt                       $IN/iso/boot/isolinux/help-release.txt
cp -av $SRC/syslinux/syslinux.cfg                                    $IN/iso/boot/isolinux/isolinux.cfg
sed \
    -e "s/__LIGHTWHALE_BOOT__/BIOS/" \
    $BINARIES_DIR/lightwhale-hello.txt >                          $IN/iso/boot/isolinux/hello.txt

# Install GRUB.
cp -av $SRC/grub/fonts/unicode.pf2                                $IN/iso/boot/grub/fonts/
cp -av $OUT/BOOTx64-minimal.EFI                                   $IN/esp/EFI/BOOT/BOOTx64.EFI
sed \
    -e "s,__LIGHTWHALE_NAME__,$LIGHTWHALE_NAME,g" \
    -e "s,__LIGHTWHALE_VERSION__,$LIGHTWHALE_VERSION,g" \
     $SRC/grub/grub.cfg >                                         $IN/iso/boot/grub/grub.cfg

sed \
    -e "s/__LIGHTWHALE_BOOT__/EFI/" \
    $BINARIES_DIR/lightwhale-hello.txt >                          $IN/iso/boot/grub/hello.txt


# Populate EFI System Partition.
RAMDISK_SIZE_B=$(stat --format="%s" $IN/iso/boot/lightwhale-rootfs)
RAMDISK_SIZE_KB=$(( ($RAMDISK_SIZE_B + 1023) / 1024 ))
sed -e s,__RAMDISK_SIZE_KB__,${RAMDISK_SIZE_KB},g -i $IN/iso/boot/isolinux/isolinux.cfg
sed -e s,__RAMDISK_SIZE_KB__,${RAMDISK_SIZE_KB},g -i $IN/iso/boot/grub/grub.cfg

ESP_SIZE_MB=$(( $(du -c -Sm $IN/esp | grep total | cut -f 1) + 2 ))
sed -e s,__ESP_SIZE_MB__,${ESP_SIZE_MB}M, $CONFIG_SRC > $CONFIG

tree $BASE
cat $CONFIG

# Build the EFI System Partition (ESP).
$HOST_DIR/bin/genimage \
    --rootpath $ROOT \
    --inputpath $IN \
    --outputpath $OUT \
    --tmppath $TEMP \
    --config $CONFIG
ls -lh $OUT/esp.vfat

cp -av $OUT/esp.vfat $IN/iso/boot/



tree $BASE



# https://www.gnu.org/software/xorriso/man_1_xorrisofs.html
iso_volid=$(echo -n "${LIGHTWHALE_NAME} ${LIGHTWHALE_VERSION}" | tr [:lower:] [:upper:] | tr -c 'A-Za-z0-9' '_')
iso_volset="${LIGHTWHALE_NAME} ${LIGHTWHALE_VERSION}"
iso_publisher="$LIGHTWHALE_PUBLISHER"

xorrisofs \
    -o $OUT/lightwhale.iso \
    -volid "$iso_volid" \
    -volset "$iso_volset" \
    -publisher "$iso_publisher" \
    -eltorito-boot boot/isolinux/isolinux.bin \
    -eltorito-catalog boot/isolinux/boot.cat \
    -no-emul-boot -boot-load-size 4 -boot-info-table \
    -isohybrid-mbr $IN/isohdpfx.bin \
    -partition_offset 16 \
    -eltorito-alt-boot -e boot/esp.vfat -no-emul-boot \
    -append_partition 2 0xef $IN/iso/boot/esp.vfat \
    $IN/iso
cp -v $OUT/lightwhale.iso $BINARIES_DIR/

#xorrisofs \
#    -o $OUT/bios.img \
#    -eltorito-boot boot/isolinux/isolinux.bin \
#    -eltorito-catalog boot/isolinux/boot.cat \
#    -no-emul-boot -boot-load-size 4 -boot-info-table \
#    $IN/iso
#cp -v $OUT/bios.img $BINARIES_DIR/lightwhale-bios.img

