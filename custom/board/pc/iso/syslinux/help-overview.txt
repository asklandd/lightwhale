------------------------------------------------------------------------------
OVERVIEW OF LIGHTWHALE KERNEL PARAMETERS

Lightwhale supports a number of kernel parameters for configuring the system.
These are particularly useful when trying out the system or trouble shooting.

The configurations are documented on separate screens:

    F1:  Overview
    F2:  Wifi
    F3:  Persistence
    F4:  Docker.
    F5:  Zram swap
    F6:  Keyboard, font, hostname, vga screen modes, other

    F10: Release info








Press ENTER to boot, type kernel parameters, or press F-keys for help.
