#!/bin/sh

echo "###############  Hello, I'm post-image.sh; $0"

set -e

BOARD_DIR=$(dirname "$0")

# Build x86 ISO image.
"$BOARD_DIR/iso/mkiso.sh"
ls -lh $BINARIES_DIR/lightwhale.iso

echo "Ok; $0"
