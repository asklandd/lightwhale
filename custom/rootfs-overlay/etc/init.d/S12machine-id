#!/bin/sh

this=S03machine-id
. /lib/lightwhale/functions
assert_root $this

case $1 in
	start) ;;
	stop)  exit 0 ;;
	*)     fail "Usage: $this {start|stop}" ;;
esac

set -u

# §1 If kernel parameter is given, use that (regardless if file exists).
# §2 Otherwise generate machine-id.
# §3 If file exists, do not overwrite it.
# §4 Unless kernel parameter was given.
# §5 generated machine-id doesn't match file,
#    and no kernel parameter was given,
#    do NOT overwrite file with generated machine-id,
#    because file may contain a user-defined machine-id
#    that is important not to overwrite.

machine_id_file="/etc/machine-id"

if [ -f "$machine_id_file" ]; then
    current="$(cat "$machine_id_file")"
else
	current=""
fi


override=$(get_kernel_parameter "machine-id")

if [ -n "$override" ]; then
	cleaned=$(echo "$override" | to_valid_hostname )
    update="$cleaned"
elif [ -z "$current" ]; then
    update="$(/lib/lightwhale/machine-id)" || fail "FAILED to generate machine-id"
else
    update="$current"
fi


if [ -n "$current" ] && [ "$current" != "$update" ]; then
    last_modified_at="$(date -r "$machine_id_file" "+%F %T")"
    echo "WARNING: Machine-id has changed:"
    echo "WARNING: Old: \"$current\", last modified $last_modified_at"
    echo "WARNING: New: \"$update\""
fi


echo -n "Configuring machine-id: "
if [ "$current" != "$update" ]; then
    echo "$update" > "$machine_id_file" || fail "FAILED to update file: $machine_id_file"
fi
echo "$update"
