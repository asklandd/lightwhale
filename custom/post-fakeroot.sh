#!/bin/bash

#
#  Post-fakeroot scripts are shell scripts that are called
#  at the end of the fakeroot phase, right before the 
#  filesystem image generator is called.
#  As such, they are called in the fakeroot context.
#
#  Post-fakeroot scripts can be useful in case you need 
#  to tweak the filesystem to do modifications that are
#  usually only available to the root user.
#
#  https://buildroot.org/downloads/manual/manual.html#rootfs-custom
#

echo "###############  Hello, I'm post-fakeroot.sh; $0"

set -uex

# Source and determine raw data.
linux_version=$(cat $O/.config | grep ^BR2_LINUX_KERNEL_VERSION= | cut -d= -f2 | tr -d \")
linux_firmware_version=$(cat $BR/package/linux-firmware/linux-firmware.mk | grep ^LINUX_FIRMWARE_VERSION | cut -d' ' -f3)
docker_engine_version=$(cat $BR/package/docker-engine/docker-engine.mk | grep ^DOCKER_ENGINE_VERSION | cut -d' ' -f3)
docker_compose_version=$(cat $BR/package/docker-compose/docker-compose.mk | grep ^DOCKER_COMPOSE_VERSION | cut -d' ' -f3)
syslinux_version=$(cat $BR/boot/syslinux/syslinux.mk | grep ^SYSLINUX_VERSION | cut -d' ' -f3)
grub_version=$(cat $BR/boot/grub2/grub2.mk | grep ^GRUB2_VERSION | cut -d' ' -f3)

cat << EOF > "$BINARIES_DIR"/lightwhale-release
LIGHTWHALE_NAME="$LIGHTWHALE_NAME"
LIGHTWHALE_VERSION="$LIGHTWHALE_VERSION"
LIGHTWHALE_BUILD_DATE="$LIGHTWHALE_BUILD_DATE"
LIGHTWHALE_GIT_HASH="$LIGHTWHALE_GIT_HASH"
LIGHTWHALE_GIT_BRANCH="$LIGHTWHALE_GIT_BRANCH"
LIGHTWHALE_PUBLISHER="$LIGHTWHALE_PUBLISHER"
LIGHTWHALE_HOME_URL="$LIGHTWHALE_HOME_URL"

LIGHTWHALE_BUILDROOT_VERSION="$LIGHTWHALE_BUILDROOT_VERSION"
LIGHTWHALE_LINUX_VERSION="$linux_version"
LIGHTWHALE_LINUX_FIRMWARE_VERSION="$linux_firmware_version"
LIGHTWHALE_DOCKER_ENGINE_VERSION="$docker_engine_version"
LIGHTWHALE_DOCKER_COMPOSE_VERSION="$docker_compose_version"
LIGHTWHALE_SYSLINUX_VERSION="$syslinux_version"
LIGHTWHALE_GRUB_VERSION="$grub_version"
EOF
ls -l "$BINARIES_DIR"/lightwhale-release



# Add Lightwhale version to SYSLINUX splash screen.
name_upper=$(echo $LIGHTWHALE_NAME | tr '[:lower:]' '[:upper:]')
welcome="WELCOME TO $name_upper ${LIGHTWHALE_VERSION}!"
welcome_length=${#welcome}
welcome_offset=$(( (80 - welcome_length) / 2))
welcome_padding=""
for ((i=0; i < welcome_offset; i++)); do welcome_padding+=" "; done
LIGHTWHALE_WELCOME="$welcome_padding$welcome"

cat "$BR2_EXTERNAL_LIGHTWHALE_PATH"/board/pc/iso/hello.txt | sed -e "s/__LIGHTWHALE_WELCOME__/$LIGHTWHALE_WELCOME/" > "$BINARIES_DIR"/lightwhale-hello.txt

cp "$BR2_EXTERNAL_LIGHTWHALE_PATH"/board/pc/iso/syslinux/help-release.txt "$BINARIES_DIR"/syslinux-help-release.txt

cat "$BINARIES_DIR"/lightwhale-release >> "$BINARIES_DIR"/syslinux-help-release.txt
actual_lines=$(cat "$BINARIES_DIR"/syslinux-help-release.txt | wc -l)
expected_lines=26 # 25+1 to push the top horizontal line out of the screen.
missing_lines=$((expected_lines - actual_lines - 1))
for n in $(seq $missing_lines); do
    echo >> "$BINARIES_DIR"/syslinux-help-release.txt
done



# Create a new os-release.
cat << EOF > "$TARGET_DIR"/usr/lib/os-release
NAME=$LIGHTWHALE_NAME
VERSION=$LIGHTWHALE_VERSION
PRETTY_NAME="$LIGHTWHALE_NAME"
ID=lightwhale
ID_LIKE=buildroot
BUILD_ID="${LIGHTWHALE_GIT_BRANCH}#${LIGHTWHALE_GIT_HASH}"

EOF
cp -v "$BINARIES_DIR"/lightwhale-release "$TARGET_DIR"/etc/lightwhale-release




#
# CLEANUP
#

# Grub is not needed on target root filesystem.
rm -fr "$TARGET_DIR"/share  # Only GRUB creates this bastard directory.
rm -fr "$TARGET_DIR"/bin/grub-*
rm -fr "$TARGET_DIR"/sbin/grub-*
rm -fr "$TARGET_DIR"/lib/grub
rm -fr "$TARGET_DIR"/etc/grub.d
rm -fr "$TARGET_DIR"/etc/bash-completion.d/grub

# Cleanup obsolete cgroup v1 init scripts installed by Buildroot.
rm -fr "$TARGET_DIR"/usr/bin/cgroupfs-mount
rm -fr "$TARGET_DIR"/usr/bin/cgroupfs-umount
rm -fr "$TARGET_DIR"/etc/init.d/S30cgroupfs
rm -fr "$TARGET_DIR"/etc/init.d/S31cgroups-hierarchy-fix

# Cleanup unused files that are installed by Buildroot.
rm -fr "$TARGET_DIR"/media
rm -fr "$TARGET_DIR"/boot
rm -fr "$TARGET_DIR"/opt
rm -fr "$TARGET_DIR"/usr/libexec/lzo
rm -fr "$TARGET_DIR"/etc/init.d/fuse3

rm -fr "$TARGET_DIR"/usr/share/pixmaps
rm -fr "$TARGET_DIR"/usr/share/icons
rm -fr "$TARGET_DIR"/usr/share/applications
rm -fr "$TARGET_DIR"/usr/share/et
rm -fr "$TARGET_DIR"/usr/share/gvfs

rm -fr "$TARGET_DIR"/usr/lib/gio
rm -fr "$TARGET_DIR"/usr/lib/gvfs
rm -fr "$TARGET_DIR"/usr/lib/xfsprogs
rm -fr "$TARGET_DIR"/usr/lib/gconv

rm -f "$TARGET_DIR"/usr/bin/sftp

rm -fr "$TARGET_DIR"/sbin/ldconfig

for git in git-cvsserver git-receive-pack git-shell git-upload-archive git-upload-pack scalar; do
    rm -fr "$TARGET_DIR"/usr/bin/$git
done
rm -fr "$TARGET_DIR"/usr/share/gitweb

for sudo in bin/cvtsudoers bin/sudoreplay bin/sudoedit sbin/sudo_logsrvd sbin/sudo_sendlog ; do
    rm -fr "$TARGET_DIR"/usr/$sudo
done

for kbd in loadunimap dumpkeys; do
    rm -fr "$TARGET_DIR"/usr/bin/$kbd
done

for gapp in gapplication gsettings gresource gio gio-querymodules; do
    rm -fr "$TARGET_DIR"/usr/bin/$gapp
done
rm -fr "$TARGET_DIR"/usr/share/GConf

for keymap in \
    amiga atari mac sun ppc pine \
    i386/oldpc i386/neo i386/fgGIod i386/colemap i386/carpalx i386/dvorak "i386/qwerty/ru*"
do
    rm -fr "$TARGET_DIR"/usr/share/keymaps/$keymap
done

for v in \
    print compiler macros plugin import tools tutor spell \
    menu.vim synmenu.vim delmenu.vim bugreports.vim optwin.vim mswin.vim
do
    rm -fr "$TARGET_DIR"/usr/share/vim/vim90/$v
done

for a in \
    RstFold ada* bitmake cargo clojure* decada freebasic \
    getscript gnat gzip haskell* javascript* netrw* php* \
    ruby* rust* spell* sql* tar* tohtml* typeset* vimball* xml* zip* zig
do
    rm -fr "$TARGET_DIR"/usr/share/vim/vim90/autoload/${a}.vim
done

(
    shopt -s extglob  # Run sub-shell with globbing.
    GLOBIGNORE=lists:default.vim:defaults.vim:scripts.vim:syntax.vim:syncolor.vim:synload.vim:dockerfile.vim:diff.vim:bash.vim:git{,config,commit,config,ignore,rebase}.vim:gpg.vim:sudoers.vim:ssh{,d}config.vim:tmux.vim:json.vim:passwd.vim:yaml.vim:python{,2}.vim:sh.vim:vim.vim:nginx.vim
    GLOBIGNORE+=:lightwhale-dummy.vim
    cd "$TARGET_DIR"/usr/share/vim/vim90/colors
    rm -fr *
    cd "$TARGET_DIR"/usr/share/vim/vim90/syntax
    rm -fr *
    cd "$TARGET_DIR"/usr/share/vim/vim90/indent
    rm -fr *
)

cat << EOF > $TARGET_DIR/usr/share/vim/vim90/syntax/lightwhale-dummy.vim
" Empty Vim syntax file to satisfy includes from other Vim syntax files,
" and prevent Vim from when editing these files.
" The real syntax files were deemed too large, unnecessary, or otherwise
" unworthy to be included in Lightwhale.
EOF

for dummy in eruby.vim lua.vim; do
    ln -sfv lightwhale-dummy.vim $TARGET_DIR/usr/share/vim/vim90/syntax/$dummy
done


rm -rv "$TARGET_DIR"/usr/share/bash-completion/helpers/perl
(
    shopt -s extglob  # Run sub-shell with globbing.
    GLOBIGNORE=bind:blkid:cpio:curl:docker:dd:dhclient:ether-wake:ethtool:find:gpg*:ip:iptables:lsblk:mdadm:mount:rsync:ssh:ssh-*:sshfs:
    cd "$TARGET_DIR"/usr/share/bash-completion/completions
    rm -fr *
)


