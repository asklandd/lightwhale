#!/bin/sh

echo "Determining file system of directory $dir... "

dir=/var/lib/docker
mount_point=$(LC_ALL=C df $dir | grep -v ^Filesystem | cut -d ' ' -f 6)
fs=$(LC_ALL=C mount | grep "on $mount_point" | cut -d ' ' -f 5)

if [ "$fs" = "ramfs" ] || [ "$fs" = "tmpfs" ]; then
	echo "YOUR $dir IS ON A RAMDISK"
	DOCKER_RAMDISK=true
fi

echo DOCKER_RAMDISK=$DOCKER_RAMDISK

