
Hello, there!
=============

Please visit the [<i>Ligtwhale main page</i>](https://lightwhale.asklandd.dk/)
for a description of the Lightwhale project,
and a full user's guide for getting started.

The actual repository here hosts the source code for Lightwhale,
and is not of any interest for most people.

Have a nice day!
